#from MadGraphControl.MadGraph_NNPDF30NLOMC_Base_Fragment import *
from MadGraphControl.MadGraph_NNPDF30NLO_NoSys_Base_Fragment import *
import re
import os
import subprocess

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

nevents = runArgs.maxEvents*5.5 if runArgs.maxEvents>0 else 1.5*evgenConfig.nEventsPerJob

print runArgs.maxEvents
print nevents

job_option_name = get_physics_short()
print job_option_name

# quark_flavour = job_option_name.split('U1')[-1][0]
quark_flavour = "b"
# quark_flavours = ['c','c~', 'b', 'b~','s','s~', 't']

# if quark_flavour not in quark_flavours and not  ("tautauLQ" in job_option_name or "nunuLQ" in job_option_name or "allLQ" in job_option_name) :
#     print("Cannot determine quark flavour from job option name: {:s}.".format(job_option_name))

#
process_name = "pp>ta+,ta-"

# Merging settings
maxjetflavor=5
ickkw=0
nJetMax=2
ktdurham=15
dparameter=0.4

# Get Mass from jo file
matches = re.search("MU([0-9]+).*", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))
print lqmass


    
# Get Kappa from jo file : For Yangs-Mills : kappa=0 ; Minimal coupling kappa=1
#matches_kappa = re.search("K([0-1]+).*", job_option_name)
#if matches_kappa is None:
#    raise RuntimeError("Cannot find kappa string in job option name: {:s}.".format(job_option_name))
#else:
#    lkappa = float(matches_kappa.group(1))
lkappa = 0.0

#Get overall coupling strength gu from jo file
matches_gu = re.search("GU([0-9]_[0-9]+).*", job_option_name)
if matches_gu is None:
    #print("Cannot find gU string in job option name: {:s} -- using default value gU =3.0".format(job_option_name))
    print("Cannot find gU string in job option name: {:s} -- using default value gU =1.414214".format(job_option_name))
    lgu=1.414214
else:
    lgu = float(matches_gu.group(1).replace("_", "."))


#Get betaL32 from jo file
matches_betaL32 = re.search("32L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL32 is None:
    print("Cannot find beta 32 string in job option name: {:s}.".format(job_option_name))
    betaL32=0.0
else:
    betaL32 = float(matches_lambda32.group(1).replace("_", "."))

#Get betaL23 from jo file
matches_betaL23 = re.search("23L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL23 is None:
    print("Cannot find beta 23 string in job option name: {:s}.".format(job_option_name))
    betaL23=0.0
else:
    betaL23 = float(matches_betaL23.group(1).replace("_", "."))

#Get betaL33 from jo file
matches_betaL33 = re.search("33L([0-9]_[0-9]+).*", job_option_name)
if matches_betaL33 is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    betaL33=0.0
else:
    betaL33= float(matches_betaL33.group(1).replace("_", "."))
    
#Get betaR33 from jo file
matches_betaR33 = re.search("33R([0-9]_[0-9]+).*", job_option_name)
if matches_betaR33 is None:
    print("Cannot find betaR 33 string in job option name: {:s}.".format(job_option_name))
    betaR33=0.0
else:
    betaR33= float(matches_betaR33.group(1).replace("_", "."))


process_1=""
process_2=""

process_def="""
    set group_subprocesses Auto
    set ignore_six_quark_processes False
    set loop_color_flows False
    set gauge unitary
    set complex_mass_scheme False
    set max_npoint_for_channel 0
    import model ./vector_LQ_UFO
    define p  = g u c d s b u~ c~ d~ s~ b~
   """

if "LQnu" in job_option_name:
    # Setup for tau + t/c nu 
    process_def+="""
    generate  p p > {finalState}  {npOrder} @1 
    add process p p > {finalStateCC}  {npOrder} @2"""
    if 'res' in job_option_name:
        process_1 = "ta- vt~ {:s} /{:s}~ ".format(quark_flavour,quark_flavour)
        process_2 = "ta+ vt {:s}~ /{:s} ".format(quark_flavour,quark_flavour)
    elif "non" in job_option_name:
        process_1 = "ta- vt~ {:s} $$vlq ".format(quark_flavour)
        process_2 = "ta+ vt {:s}~ $$vlq~ ".format(quark_flavour)
    else:
        process_1 = "ta- vt~ {:s} ".format(quark_flavour)
        process_2 = "ta+ vt {:s}~ ".format(quark_flavour)

 
elif "LQtau" in job_option_name:
    # Setup for nu + b/s tau
    process_def+="""
    generate  p p > {finalState}  {npOrder} @1 
    add process p p > {finalStateCC}  {npOrder} @2"""
    #process_1 = "ta+ vt {:s}".format(quark_flavour)
    #process_2 = "ta- vt~ {:s}~".format(quark_flavour)
    process_1 = "ta+ vt j"
    process_2 = "ta- vt~ j"


elif "LQ2tau" in job_option_name:
    # Setup for tau + b/s tau 
    process_def+="""
    generate  p p > {finalState}  {npOrder} @1 
    add process p p > {finalStateCC}  {npOrder} @2"""
    process_1 = "ta- ta+ {:s}".format(quark_flavour)
    process_2 = "ta+ ta- {:s}~".format(quark_flavour)
    
    
elif "tautau" in job_option_name:
    print("tautau is selected.")
    # Setup for non res and remove  cc process due to symmetry
    process_1 = "ta- ta+"
    process_def+="""
    generate  p p > {finalState} {npOrder}
    add process  p p > {finalState} j {npOrder}
    add process  p p > {finalState} j j {npOrder}"""

    
elif "nunuLQ" in job_option_name:
    # Setup for non res  and remove  cc process due to symmetry

    process_1 = "nu nu~"
    process_def=process_def.replace("add process p p > {finalStateCC} {npOrder}", "")


  
process_def+=""" 
output -f"""
process_def_run= process_def.replace("{finalState}", process_1).replace("{finalStateCC}",process_2)


    
process_def_SM            = process_def_run.replace("{npOrder}", "NP==0")
process_def_allNonSM      = process_def_run.replace("{npOrder}", "NP^2>0")
process_def_interfer      = process_def_run.replace("{npOrder}", "NP^2==2")
process_def_BSM           = process_def_run.replace("{npOrder}", "NP==2")
process_def_all           = process_def_run.replace("{npOrder}", "")
process_def_allNonSMres   = process_def_run.replace("{npOrder}", "WEIGHTED<=8")
# Get beam energy
beamEnergy = -999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

if  "inf" in job_option_name :
    process_dir = new_process(process_def_interfer)
elif  "bsm" in job_option_name :
    process_dir = new_process(process_def_BSM)
elif  "bi" in job_option_name :
    process_dir = new_process(process_def_allNonSM)
else:
    process_dir = new_process(process_def_allNonSM)

# Get mass_low from jo file
slice_low  = 0
slice_high = 0 
matches_slice = re.search("SL([0-9]+).*", job_option_name)
if matches_slice is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    slice_low=0.0
else:
    slice_low= float(matches_slice.group(1))
if(slice_low==0. or slice_low==200.):
    slice_high = 500 # upper limit for dilepton mass
elif(slice_low==500.):
    slice_high = 1000 # upper limit for dilepton mass
elif(slice_low==2000.):
    slice_high = -1 # upper limit for dilepton mass
else:
    slice_high = slice_low + 1000. # upper limit for dilepton mass

extras = {
    'lhe_version'   : '3.0',
    'sde_strategy' : 1,
    'ickkw': int(ickkw),
    'xqcut': 0,
    'asrwgtflavor': int(maxjetflavor),
    'maxjetflavor': int(maxjetflavor),
    'ktdurham': ktdurham,
    'dparameter': dparameter,
    'ihtmin' : 0.0,
#    'misset' : 200.0,
    'misset' : 0.0,
    'cut_decays' : 'F',
    'nevents' : int(nevents),
    'ptj':'15.0',
    'ptb':'15.0',
    'etab':'5.0',
    'etaa':'5.0',
    'etal':'5.0',
    'drjj':'0.0',
    'drll':'0.0',
    'draa':'0.0',
    'draj':'0.0',
    'drjl':'0.0',
    'dral':'0.0',
    'mmll':slice_low,
    'mmllmax': slice_high,
    'dynamical_scale_choice':'4',
}


#----------------------------------------------------------------------------------------
# Setting  model parameters
#----------------------------------------------------------------------------------------
#---Define model parameter: - for LQ production no couplings to Z_prime and heavy gluon
#                           - beta23 and beta33 taken from job_option_name (encoded cf table)
#                            

vLQ_par={
    'gU' :  '{:e}' .format(lgu) ,
    'betaL33': '{:e}'  .format(betaL33) ,
    'betaRd33' : '{:e}'.format(betaR33),
    'betaL23' : '{:e}' .format(betaL23),
    'betaL32' : '{:e}' .format(betaL32),
    'kappaU' : '{:e}' .format(lkappa),
    
    }
zP_par={
    'gZp' :  '{:e}' .format(0.0) ,
    'zetaq33': '{:e}' .format(0.0) ,
    'zetal33' : '{:e}'.format(0.0),
    'zetaRu33' : '{:e}' .format(0.0),
    'zetaRd33' : '{:e}' .format(0.0),
    'zetaRe33' : '{:e}' .format(0.0),
    'zetaqll' : '{:e}' .format(0.0),
    'zetal22' : '{:e}' .format(0.0),
    'zetal23' : '{:e}' .format(0.0),
    'zetaRull' : '{:e}' .format(0.0),
    'zetaRdll' : '{:e}' .format(0.0),
    'zetaRe22' : '{:e}' .format(0.0),
    
    }
gP_par={
    'gGp' :  '{:e}' .format(0.0) ,
    'kappaq33': '{:e}'  .format(0.0) ,
    'kappaRu33' : '{:e}'.format(0.0),
    'kappaRd33' : '{:e}' .format(0.0),
    'kappaqll': '{:e}'  .format(0.0) ,
    'kappaRull' : '{:e}'.format(0.0),
    'kappaRdll' : '{:e}' .format(0.0),
    'kappaG1' : '{:e}'.format(0.0),
    'kappaG2' : '{:e}' .format(0.0),
    
    }

vLQ_Mass={
    '9000006' : '{:e}' .format(1e9),
    '9000006' : '{:e}' .format(1e9),
    '9000007' : '{:e}' .format(lqmass),
    }
vLQ_Width={
    '9000007' : 'Auto',
    }
 
params = {}

params['NPLQCOUP']= vLQ_par
params['NPGPCOUP']= gP_par
params['NPZPCOUP']= zP_par
params['MASS'] = vLQ_Mass
params['DECAY'] = vLQ_Width

modify_param_card(process_dir=process_dir, params=params)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

## MadSpin
#madspin_card = process_dir+'/Cards/madspin_card.dat'
#if os.access(madspin_card,os.R_OK):
    #os.unlink(madspin_card)
#mscard = open(madspin_card, 'w')
#mscard.write("""#************************************************************
###*                        MadSpin                           *
###*                                                          *
###*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer * 
###*                                                          *
###*    Part of the MadGraph5_aMC@NLO Framework:              *
###*    The MadGraph5_aMC@NLO Development Team - Find us at   *
###*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
###*                                                          *
###*    Manual:                                               *
###*    cp3.irmp.ucl.ac.be/projects/madgraph/wiki/MadSpin     *
###*                                                          *
###************************************************************
###Some options (uncomment to apply)
###
#set seed %i
## set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
## set BW_cut 15                 # cut on how far the particle can be off-shell
## set spinmode onshell          # Use one of the madspin special mode
#set max_weight_ps_point 400  # number of PS to estimate the maximum for each event

## specify the decay for the final state particles
#decay t > w+ b, w+ > all all
#decay t~ > w- b~, w- > all all
#decay w+ > all all
#decay w- > all all
#decay z > all all
### running the actual code
#launch"""%runArgs.randomSeed)
#mscard.close()

pdgfile = open("pdgid_extras.txt" , "w+")
pdgfile.write("""9000007
    -9000007
""")
pdgfile.close()


# reweight_card=process_dir+'/Cards/reweight_card.dat'
# reweight_card_f = open(reweight_card,'w')
# reweight_card_f.write("""
#    use_eventid True
#    change mode LO+NLO
#    """)
# b23=0.1
# while b23 < 1.2:
#    b33=0.0
#    while b33 < 1.2:
#        reweight_card_f.write("""
#        launch --rwgt_name=23L{:s}_33L{:s}
#            set NPLQCOUP  4 {:e}
#            set NPLQCOUP  2 {:e}""".format(str(b23).replace(".", "_"),str(b33).replace(".", "_" ),b23,b33))
#        b33=b33+0.1
#    b23=b23+0.1
# reweight_card_f.close()


print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs, lhe_version=3, saveProcDir=True)
#----------------------------------------------------------------------------------------
# EVGEN configuration
#----------------------------------------------------------------------------------------

evgenConfig.description = 'Vector LO single production of U1 to, : {0:s} , mLQ={1:d}'.format( quark_flavour, int(lqmass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark']
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp>vlqtau'
evgenConfig.contact = ["Patrick Bauer <patrick.bauer@cern.ch>"]
                                                                                                                                               
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# CKKWL setting
PYTHIA8_nJetMax=nJetMax
PYTHIA8_TMS=ktdurham
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process=process_name
PYTHIA8_nQuarksMerge=maxjetflavor
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
genSeq.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on"]
