import re
import os
import math
import subprocess

from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtils import remap_lhe_pdgids

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
job_option_name = get_physics_short()

generation = 0
quark_flavour = job_option_name.split('S1T')[-1][0]
quark_flavours = ['d', 's', 'b']
quark_flavour_index = quark_flavours.index(quark_flavour) + 1
if quark_flavour not in quark_flavours:
    raise RuntimeError("Cannot determine quark flavour from job option name: {:s}.".format(job_option_name))
if "LQe" in job_option_name:
    generation = 1
elif "LQmu" in job_option_name:
    generation = 2
elif "LQtau" in job_option_name:
    generation = 3
else:
    raise RuntimeError("Cannot determine LQ generation from job option name: {:s}.".format(job_option_name))

#matches = re.search("M([0-9]+).*\.py", job_option_name)
matches = re.search("M([0-9]+).*", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    lqmass = float(matches.group(1))

#matches = re.search("l([0-9]_[0-9]+)\.py", job_option_name)
matches = re.search("l([0-9]_[0-9]+)", job_option_name)
if matches is None:
    raise RuntimeError("Cannot find coupling string in job option name: {:s}.".format(job_option_name))
else:
    coupling = float(matches.group(1).replace("_", "."))

my_process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set loop_color_flows False
set gauge unitary
set complex_mass_scheme False
set max_npoint_for_channel 0
import model sm
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~\n"""

if generation == 1:
    my_process += """import model LO_LQ_S1Tilde\n"""
    if quark_flavour_index == 3:
        my_process += """define p = g u c d s b u~ c~ d~ s~ b~\n"""
    my_process += """generate p p > e+ e- npOrder \n"""
    my_process += """output -f\n"""

elif generation == 2:
    my_process += """import model LO_LQ_S1Tilde\n"""
    if quark_flavour_index == 3:
        my_process += """define p = g u c d s b u~ c~ d~ s~ b~\n"""
    my_process += """generate p p > mu+ mu- npOrder \n"""
    my_process += """output -f\n"""

elif generation == 3:
    my_process += """import model LO_LQ_S1Tilde\n"""
    my_process += """define p = g u c d s b u~ c~ d~ s~ b~\n"""
    my_process += """generate p p > ta+ ta- npOrder \n"""
    my_process += """output -f\n"""

process_def_SM               = my_process.replace( "npOrder", "NP==0" ) # only SM
process_def_allNonSM         = my_process.replace( "npOrder", "NP^2>0" ) # I + BSM
process_def_interferenceOnly = my_process.replace( "npOrder", "NP^2==2" ) # only I
process_def_pureBSM          = my_process.replace( "npOrder", "NP==2" ) # only BSM
process_def_all              = my_process.replace( "npOrder", "" ) # only BSM

#process_dir = new_process(my_process)
process_dir = new_process(process_def_all)
settings = {'lhe_version': '3.0',
            'pdlabel': "'lhapdf'",
            'lhaid': 260000,
            'ickkw': 0,
            'nevents': nevents,
            'dynamical_scale_choice': 4,
            'mmll': slice_low,
            'mmllmax': slice_high,
            'drll': 0.,
            'ptl': 0,
            'etal': -1,}
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=settings)


if os.path.exists("param_card.dat"):
    os.remove("param_card.dat")


params = {}
params['LQPARAM'] = {'MS1t': '{:e}'.format(lqmass)}
params['MASS'] = {'s1tm43': '{:e}'.format(lqmass)}
couplings_fixed = 0.0
params['YUKS1tRR'] = {'yRR1x1': '{:e}'.format(couplings_fixed), 
                      'yRR1x2': '{:e}'.format(couplings_fixed), 
                      'yRR1x3': '{:e}'.format(couplings_fixed), 
                      'yRR2x1': '{:e}'.format(couplings_fixed), 
                      'yRR2x2': '{:e}'.format(couplings_fixed), 
                      'yRR2x3': '{:e}'.format(couplings_fixed), 
                      'yRR3x1': '{:e}'.format(couplings_fixed), 
                      'yRR3x2': '{:e}'.format(couplings_fixed), 
                      'yRR3x3': '{:e}'.format(coupling)}

# If possible, build the param card from the one that comes with the model
modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(runArgs=runArgs, process_dir=process_dir, lhe_version=3, saveProcDir=True)
# remap the PDG ID in the LHE file after its generation
remap_lhe_pdgids(runArgs.inputGeneratorFile+".events",pdgid_map={9000005:42})

evgenConfig.description = 'Toolbox scalar LO single production of S1~, generation: {0:d}, mLQ={1:d}'.format(
    int(generation), int(lqmass))
evgenConfig.keywords += ['BSM', 'exotic', 'leptoquark', 'scalar']

evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.process = 'pp -> S1~ l'
evgenConfig.contact = ["Marcus Matthias Morgenstern <marcus.matthias.morgenstern@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")  
