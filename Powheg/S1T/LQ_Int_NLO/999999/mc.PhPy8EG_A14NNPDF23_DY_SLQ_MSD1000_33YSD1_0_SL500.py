# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# This is an example joboption to generate events with Powheg
# using ATLAS' interface. Users should optimise and carefully
# validate the settings before making an official sample request.
#--------------------------------------------------------------
import os
import re

THIS_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
job_option_name = [ f for f in os.listdir(THIS_DIR) if (f.startswith('mc') and f.endswith('.py'))][0]
print(job_option_name)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Drell-Yan Scalar LeptoQuark production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["BSM", "leptoquark"]
evgenConfig.contact = ["tpelzer@cern.ch"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Drell-Yan Scalar LeptoQuark process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_DY_SLQ_Common.py")

# --------------------------------------------------------------
# Relevant parameters for this process
# --------------------------------------------------------------
# Yukawa couplings to down-type quarks (S1t RR)
PowhegConfig.YSD1x1 = 0.
PowhegConfig.YSD1x2 = 0.
PowhegConfig.YSD1x3 = 0.
PowhegConfig.YSD2x1 = 0.
PowhegConfig.YSD2x2 = 0.
PowhegConfig.YSD2x3 = 0.
PowhegConfig.YSD3x1 = 0.
PowhegConfig.YSD3x2 = 0.

# Get YSD33 from jo file
matches_YSD33 = re.search("33YSD([0-9]_[0-9]+).*", job_option_name)
if matches_YSD33 is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    YSD33=0.0
else:
    YSD33= float(matches_YSD33.group(1).replace("_", "."))
PowhegConfig.YSD3x3 = YSD33

# Get MSD from jo file
matches_MSD = re.search("MSD([0-9]+).*", job_option_name)
if matches_MSD is None:
    raise RuntimeError("Cannot find mass string in job option name: {:s}.".format(job_option_name))
else:
    MSD = float(matches_MSD.group(1))
print(MSD)
PowhegConfig.MSD = MSD # mass

# Yukawa couplings to up-type quarks (S1 RR)
PowhegConfig.YSU1x1 = 0.
PowhegConfig.YSU1x2 = 0.
PowhegConfig.YSU1x3 = 0.
PowhegConfig.YSU2x1 = 0.
PowhegConfig.YSU2x2 = 0.
PowhegConfig.YSU2x3 = 0.
PowhegConfig.YSU3x1 = 0.
PowhegConfig.YSU3x2 = 0.
PowhegConfig.YSU3x3 = 0.
PowhegConfig.MSU = 2e3 # mass
# General Leptoquark (LQ) Parameters
PowhegConfig.bornonly = 0 # NOT Include NLO 
PowhegConfig.smartsig = 0
PowhegConfig.SM = 0 # Include SM contribution
PowhegConfig.LQ = 1 # Include basic LQ contributions
PowhegConfig.LQ_EW = 0 # Include LQ corrections to photon/Z couplings
PowhegConfig.LQ_Int = 1 # Include the interference between the SM and the LQ contributions
PowhegConfig.mass_t = 172.5 # top-quark (running) mass

# Get mass_low from jo file
matches_slice = re.search("SL([0-9]+).*", job_option_name)
if matches_slice is None:
    print("Cannot find betaL 33 string in job option name: {:s}.".format(job_option_name))
    slice_low=0.0
else:
    slice_low= float(matches_slice.group(1))
PowhegConfig.mass_low = slice_low # lower limit for dilepton mass
if(slice_low==500.):
    PowhegConfig.mass_high = 1000 # upper limit for dilepton mass
elif(slice_low==5000.):
    PowhegConfig.mass_high = -1 # upper limit for dilepton mass
else:
    PowhegConfig.mass_high = slice_low + 1000. # upper limit for dilepton mass
#PowhegConfig.runningscale = 1
#PowhegConfig.new_damp = 1
#PowhegConfig.hnew_damp = 0.5
#PowhegConfig.hdamp = 1.0

# --------------------------------------------------------------
# Integration settings
# --------------------------------------------------------------
PowhegConfig.ncall1 		= 10000
PowhegConfig.ncall2 		= 10000
PowhegConfig.nubound 		= 10000


# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
# Setting the appropriate number of final state particles for the main31 routine
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
